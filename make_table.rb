#!/usr/bin/ruby
require "stringio"

class MakeTable
  attr_accessor :dataHashArray
  attr_accessor :write_title
  #------------Constructor--------------
  def initialize(key_list)

    # hash 
    @dataHashArray = Hash.new

    # create array for each key
    key_list.each do |key|
      @dataHashArray[key] = []
    end

    # default value 
    @write_title = true
  end

  #------------read from input----------
  def read(input = STDIN)
    while line = input.gets
      fields = line.split(/:/)
      @dataHashArray.each_key do |key|
        if line =~ Regexp.new(key)
          @dataHashArray[key] << fields[-1].chomp.strip
        end
      end
    end
  end  

  #------------write table (string)-----
  def table_str
    res = ""

    # write title if necessary
    if @write_title
      res << @dataHashArray.keys.inject("") {|i,j| 
        i <<  j << ","}.slice(0..-2)
      res << "\n"
    end

    # write data
    transpose(@dataHashArray.values).each do |ary|
      res << ary.inject("") {|i,j| i << j << ","}.slice(0..-2)
      res << "\n"
    end

    # return string
    res
  end

  #------------OLD CODE-----------------
  def write_table_old
    primeDataNum = @dataHash[@keyArray[0]].size
    if @write_title
      print "# "
      @keyArray.each do |key|
        #        print @nameHash[key].chomp, "\t"
        print key.chomp, "\t"
      end
      print "\n"
    end
    
    for i in 0..(primeDataNum - 1)
      @keyArray.each do |key|
        print @dataHash[key][i].chomp, "\t"
      end
      print "\n"
    end
  end
  def get_data_num_old
    return @dataHash[@keyArray[0]].size
  end

  #------------Utility-----------------
  def transpose (matrix)
    # a_ij -> b_ij = a_ji
    size = if matrix.map {|ary| ary.length}.inject {|i,j| i==j} 
             matrix[0].length 
           else 
             raise "matrix is not correct."
           end
    
    Array.new(size) do |i| 
      matrix.map {|ary| ary[i]}
    end
    
  end
end

# cui
def cui
  
  if File.exist? ARGV[0]
    input = File.open ARGV[0]
    ARGV.shift
  else
    input = STDIN
  end
  
  if ARGV[0] == "-nt"
    ARGV.shift
    with_title = false
  else
    with_title = true
  end
  
  begin
    if ARGV.length == 0
      raise "you need to select key words as options."
    end
  rescue => exc
    p exc
  else
    mk = MakeTable.new ARGV
    mk.write_title = with_title
    mk.read(input)
    puts mk.table_str
  end
end










