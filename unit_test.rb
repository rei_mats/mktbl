require "./make_table.rb"
require 'minitest/unit'
#require "test/unit"
#mt = MakeTable.new([1,2,3])
#p mt

MiniTest::Unit.autorun

class MakeTableTest < MiniTest::Unit::TestCase
  def setup 
    str = <<EOS
This is contents.
key1 : val1a
key2 : val2a
pekepeke
key1 : val1b
key2 : val2b
aaaa
EOS
    sio = StringIO.new(str, 'r+')
    @mt = MakeTable.new(["key1", "key2"])
    @mt.read(sio)
  end

  def test_read
    assert_equal ["val1a", "val1b"], @mt.dataHashArray["key1"]
  end
  def test_write
    correct_str = <<EOS
key1,key2
val1a,val2a
val1b,val2b
EOS
    assert_equal correct_str, @mt.table_str
  end
  
end


