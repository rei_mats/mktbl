TARGET = mktbl
DEST = ~/bin

install : make_table.rb mktbl.sh makefile
	cp make_table.rb ${DEST}/
	cp mktbl.sh ${DEST}/mktbl
	chmod u+x ${DEST}/mktbl

test : 
	ruby unit_test.rb
